var aws = require("aws-sdk");
var nodemailer = require("nodemailer");

var ses = new aws.SES();
var s3 = new aws.S3();

exports.handler = function (event, context, callback) {

    var mailOptions = {
        from: "arpita@capitalnumbers.com",
        subject: "This is an ema il sent from a Lambda function!",
        html: `<p>Hey! Seriously you rceived email from me</p>`,
        to: "radu@allegrow.co",
        // bcc: Any BCC address you want here in an array,
    };

    // create Nodemailer SES transporter
    var transporter = nodemailer.createTransport({
        SES: ses
    });

    // send email
    transporter.sendMail(mailOptions, function (err, info) {
        if (err) {
            console.log("Error sending email");
            callback(err);
        } else {
            console.log("Email sent successfully");
            callback();
        }
    });
};